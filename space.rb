
#The space class holds infomation for a single space on the board
class Space

  #all external code to read and write the `value` variable
  attr_accessor :value

  #returns whether the space is empty or not. if the `value` is nil (unset) then
  #this space has not been played in yet
  def empty?
    @value.nil?
  end

  #converts this space to a string to be printed out. The means it will be the
  #value of `value` or a space if `value` is unset
  def to_s
    @value || ' '
  end

end
