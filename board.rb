#Include both of theses files so we can use the classes they define
require './player'
require './space'

class Board

  #When a new Board is created this code will be run.
  def initialize
    #Make two new Player objects
    @player1 = Player.new('x', 'whats the first players name')
    @player2 = Player.new('o', 'whats the second players name')
  end

  def begin_game
    #setup the initial state of the game.
    setup_game
    #Loop forever
    loop do
      #Make a single turn
      make_turn
      #If the game is finished then exit the loop if not then just continue
      break if game_over?
      #Go to the next turn
      next_turn
    end
    #Print the final screen
    print_game_over
  end

  #All the following functions are private
  #this is because code outside this class do not need to be able to call these.
  private

  def setup_game
    #Default to plater ones go
    @players_1_go = true

    #Initialize all the spaces of the board
    initialize_spaces
  end

  def make_turn
    #This just add a new line to make the layout easier to read.
    puts
    #Draw the board to the console.
    draw_board

    #Print whos turn it is and instructions for that player
    puts "#{current_player.name}[#{current_player.symbol}] it is your go"
    puts 'what space do you want to go? [1-9]'

    #get the space they want to play in.
    # subtract 1 because arrays start at 0 not 1.
    space_index = gets.chomp.to_i - 1

    #while that space isnt taken do this loop
    while space_index > 8 || space_index < 0 || !@spaces[space_index].empty?
      #print an error message
      puts 'Sorry that space is invalid'
      #get the new space they want to play
      space_index = gets.chomp.to_i - 1
    end
    #set the value of that space to the current players symbol
    @spaces[space_index].value = current_player.symbol
    puts
  end

  def game_over?
    #The game is over if either a person won or all the spaces have been played
    person_won? || max_turns_reached?
  end

  def person_won?
    #get the values of all the spaces
    current_values = @spaces.map { |space| space.value }
    #Logic for if there is three in a row
    3.times do |i|
      #ROWS
      return true if !current_values[i*3].nil? && current_values[i*3] == current_values[i*3+1] && current_values[i*3+1] == current_values[i*3+2]
      #COLUMNS
      return true if !current_values[0+i].nil? && current_values[0+i] == current_values[3+i] && current_values[3+i] == current_values[6+i]
    end
    #DIAGONALLY \
    return true if !current_values[4].nil? && current_values[0] == current_values[4] && current_values[4] == current_values[8]
    #DIAGONALLY /
    return true if !current_values[4].nil? && current_values[2] == current_values[4] && current_values[4] == current_values[6]
    #return false if none of the winning conditions were met
    return false
  end

  def max_turns_reached?
    #count the amount of empty spaces - if its 0 then the max amount of turns
    #have been played
    @spaces.count { |space| space.empty? } == 0
  end

  def print_game_over
    #if someone won then print victory
    puts "_______________________"
    puts
    draw_board
    if person_won?
      print_victory
    else
      #else print the draw screen
      print_draw
    end
  end

  def print_victory
    #print who one the game
    puts
    puts "CONGRATULATION #{current_player.name}. YOU WIN!"
    puts
  end

  def print_draw
    #print that it was a draw
    puts
    puts 'It was a good game, but alas a draw.'
    puts
  end

  def next_turn
    #set the `players_1_go` value to the opposite of what it currently is
    @players_1_go = !@players_1_go
  end

  def current_player
    #if `players_1_go` is true then return `player1` else return `player2`
    @players_1_go ? @player1 : @player2
  end


  def initialize_spaces
    #Create an empty array called spaces
    @spaces = []
    #Loop 9 times
    9.times do
      #Add a new `Space` to the spaces array.
      @spaces << Space.new
    end
  end

  def draw_board
    #loop through the spaces and print each one with the needed guide lines
    @spaces.each_with_index do |space, i|
      print space
      if (i+1)%3==0 and i!=8
        puts ''
        puts '----------'
      elsif i!=8
        print ' | '
      end
    end
    puts
    puts
  end


end
