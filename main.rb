#Run this file to start the program

#include the code from the `board` file.
require './board.rb'

#Instantiate a new `Board` object.
board = Board.new

#Call the `begin_game` method on the board object.
board.begin_game
