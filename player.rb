#The player class stores the name of the player and what symbol they are using.
class Player

  #allow the variables `symbol` and `name` to be read from external classes
  attr_reader :symbol, :name

  #The initialize method runs when you create a new instance of the class
  # this method accepts two arguments
  # symbol - what letter to display for this player. ('o' or 'x')
  # msg - what to print out when asking for their name - if no argument is
  #       passed then it will default to 'what is your name?'
  def initialize symbol, msg="what is your name?"

    #store the variable `symbol` in the object
    @symbol = symbol

    #print out to the console - the value of the variable `msg` then a ': '
    print "#{msg}: "

    #get the value of what the user entered and store it in the object under a
    #variable called name
    @name = gets.chomp
  end


end
